import { Logger } from 'pino';
import multer from 'multer';
import OcrRequestController from './Controller';
import Interactor from '../interactor/Interactor.interface';

// type Mockify<T> = {
//     [P in keyof T]: jest.Mock<{}>;
// };

jest.mock('../app');
jest.mock('pino');
jest.mock('../interactor/Interactor.interface');

describe('Request controller', () => {
    let controller: OcrRequestController;
    let interactor: Interactor;
    let logger: any;
    let resJson: jest.SpyInstance;
    let req: any;
    let res: any;
    let next: any;
    let upload: multer.Multer;

    beforeEach(function setup() {
        interactor = {
            imageProcess: jest.fn().mockResolvedValue('OCR result'),
            getResults: jest.fn(async () => [
                {
                    url: 'www.google.com',
                    text: 'OCR result',
                    _id: '123abc',
                },
            ]),
        };

        logger = {
            trace: jest.fn(),
        };

        controller = new OcrRequestController((interactor as any) as Interactor, (logger as any) as Logger, upload);
        req = {
            file: { filename: 'filename' },
        };
        res = {
            json: () => null,
        };
        resJson = jest.spyOn(res, 'json');
    });

    it('should handle post method correctly', async () => {
        await controller.processImage(req, res, next);
        expect(interactor.imageProcess).toBeCalledTimes(1);
        expect(resJson).toHaveBeenCalledWith({ result: 'OCR result' });
    });
});
