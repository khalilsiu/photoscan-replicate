import { Router, Request, Response, NextFunction } from 'express';
import { Logger } from 'pino';
// import { upload } from '../app';
import multer from 'multer';
import Interactor from '../interactor/Interactor.interface';

export default class OcrRequestController {
    constructor(private interactor: Interactor, private logger: Logger, private upload: multer.Multer) {
        this.interactor = interactor;
        this.logger = logger;
        this.upload = upload;
    }

    public router() {
        const router = Router();
        router.post('/image', this.upload.single('photo'), this.processImage);
        router.get('/results', this.getResults);
        return router;
    }

    public processImage = async (req: Request, res: Response, next: NextFunction) => {
        try {
            this.logger.trace('processImage');
            if (req.file) {
                const { filename } = req.file;
                const result = await this.interactor.imageProcess(filename);
                this.logger.trace('processImage %o', result);
                res.json({ result });
            } else {
                throw new Error('Please upload a file.');
            }
        } catch (e) {
            next(e);
        }
    };

    private getResults = async (req: Request, res: Response, next: NextFunction) => {
        try {
            this.logger.trace('getResults');
            const results = await this.interactor.getResults();
            this.logger.trace('getResults %o', results);

            res.json({ results });
        } catch (e) {
            next(e);
        }
    };
}
