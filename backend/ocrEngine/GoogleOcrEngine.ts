import fetch from 'cross-fetch';
import { Logger } from 'pino';
import OcrEngine from './OcrEngine.interface';

const envConfig = require('../env');

const { API_KEY } = envConfig;

export default class GoogleOcrEngine implements OcrEngine {
    constructor(private logger: Logger) {
        this.logger = logger;
    }

    public textDetection = async (input: { base64: string; url: string }) => {
        this.logger.trace('textDetection');

        const res = await this.googleOcrRequest(input.base64);
        const response = await res.json();

        const result = { url: input.url, text: '' };
        if (response.error) throw new Error(response.error.message);
        else if (!response.responses[0].fullTextAnnotation) {
            result.text = 'No text is detected';
        } else {
            result.text = response.responses[0].fullTextAnnotation.text;
        }
        this.logger.trace('textDetection %o', result);
        return result;
    };

    private googleOcrRequest = async (base64: string) => {
        return fetch(`https://vision.googleapis.com/v1/images:annotate?key=${API_KEY}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                requests: [
                    {
                        image: {
                            content: base64,
                        },
                        features: [
                            {
                                type: 'TEXT_DETECTION',
                            },
                        ],
                    },
                ],
            }),
        });
    };
}
