export default interface OcrEngine {
    textDetection: (input: { base64: string; url: string }) => Promise<{ url: string; text: string }>;
}
