/* eslint no-use-before-define: 0 */
import * as firebase from 'firebase-admin';
import { ServiceAccount } from 'firebase-admin';
import uniqid from 'uniqid';
import fetch from 'cross-fetch';
import { Logger } from 'pino';
import config from '../gcs.json';
import FileStorage from './FileStorage.interface';

export default class GCSStorage implements FileStorage {
    private STORAGE: firebase.storage.Storage;

    constructor(private logger: Logger) {
        firebase.initializeApp({
            credential: firebase.credential.cert(config as ServiceAccount),
        });
        this.STORAGE = firebase.storage();
        this.logger = logger;
    }

    public async _handleFile(
        req: any,
        file: Express.Multer.File,
        callback: (error?: any, info?: Partial<Express.Multer.File> | undefined) => void,
    ) {
        this.logger.trace('_handleFile', file);

        const fileName = `${uniqid()}.${file.mimetype.split('/')[1]}`;
        const bucket = this.STORAGE.bucket(`${config.project_id}.appspot.com`);
        const GCSFile = bucket.file(fileName);
        const writeStream = GCSFile.createWriteStream({
            metadata: {
                contentType: file.mimetype,
            },
        });

        file.filename = fileName;

        file.stream.pipe(writeStream);

        writeStream.on('error', callback);

        writeStream.on('finish', () => {
            this.logger.trace('File uploaded.')
            callback();
        });
    }

    public _removeFile() {}

    public getImageUrl = async (identifier: string) => {
        this.logger.trace('getImageUrl');

        const url = `https://firebasestorage.googleapis.com/v0/b/${config.project_id}.appspot.com/o/${identifier}?alt=media`;
        const res = await fetch(url);
        const response = await res.arrayBuffer();
        const imageInfo = { base64: Buffer.from(response).toString('base64'), url };
        this.logger.trace('getImageUrl %o', imageInfo.url);

        return imageInfo;
    };
}
