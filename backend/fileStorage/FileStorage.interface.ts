export default interface FileStorage {
    getImageUrl: (identifier: string) => Promise<{ base64: string; url: string }>;
}
