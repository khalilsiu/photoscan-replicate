export default interface DatabaseGateway {
    getData: () => Promise<{ url: string; text: string; _id: string }[]>;
    saveData: (input: { url: string; text: string }) => Promise<void>;
}
