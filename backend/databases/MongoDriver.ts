import mongoose from 'mongoose';
import { Logger } from 'pino';
import Result from './mongodb/models/result';
import Database from './Database.interface';

const envConfig = require('../env');

const { MONGO_URI } = envConfig;

export default class MongoDriver implements Database {
    constructor(private logger: Logger) {
        mongoose.connect(MONGO_URI, {
            useUnifiedTopology: true,
        });
        this.logger = logger;
    }

    public getData = async () => {
        this.logger.trace('getData');

        const data = await Result.find();
        const dataObj = data.map((item) => item.toObject());
        this.logger.trace('getData %o', dataObj);

        return dataObj;
    };

    public saveData = async (input: { url: string; text: string }) => {
        this.logger.trace('saveData');

        const resultEntry = new Result(input);
        await resultEntry.save();
    };
}
