import * as mongoose from 'mongoose';

const resultSchema = new mongoose.Schema({
    url: String,
    text: String,
});

export default mongoose.model('Result', resultSchema);
