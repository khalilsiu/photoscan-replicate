import express, { Response, Request, ErrorRequestHandler } from 'express';
import multer from 'multer';
import * as bodyParser from 'body-parser';
import cors from 'cors';
import pino from 'pino';
import expressPino from 'express-pino-logger';
import Interactor from './interactor/Interactor';
import MongoDriver from './databases/MongoDriver';
import Controller from './controller/Controller';
import GoogleOcrEngine from './ocrEngine/GoogleOcrEngine';
import GCSStorage from './fileStorage/GCSStorage';

const envConfig = require('./env');

const { LOG_LEVEL } = envConfig;
const logger = pino({ level: LOG_LEVEL || 'info' });
const gcsStorage = new GCSStorage(logger);
const upload = multer({ storage: gcsStorage, limits: { fieldSize: 25 * 1024 * 1024 } });

const expressLogger = expressPino(logger);
const app = express();
app.use(cors());
app.use(expressLogger);
app.use(bodyParser.urlencoded({ extended: true, limit: '50mb' }));
app.use(bodyParser.json());

const mongoDriver = new MongoDriver(logger);
const ocrEngine = new GoogleOcrEngine(logger);
const interactor = new Interactor(mongoDriver, ocrEngine, gcsStorage, logger);
const controller = new Controller(interactor, logger, upload);

app.use('/', controller.router());

app.get('/healthz', function healthCheck(req, res) {
    res.send('Health check passed');
});

app.get('/bad-health', function healthCheckFailed(req, res) {
    res.status(500).send('Health check did not pass');
});

function errorHandler(e: ErrorRequestHandler, req: Request, res: Response) {
    res.status(500);
    logger.error(e);
    res.json({ error: e.toString() });
}

app.use(errorHandler);
const PORT = 8888;

app.listen(PORT, () => {
    logger.trace(`Listening on port:${PORT}`);
});
