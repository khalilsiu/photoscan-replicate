export default interface OcrResultRequester {
    imageProcess: (filename: string) => Promise<string>;
    getResults: () => Promise<{ url: string; text: string; _id: string }[]>;
}
