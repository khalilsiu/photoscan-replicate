import { Logger } from 'pino';
import Database from '../databases/Database.interface';
import OcrEngine from '../ocrEngine/OcrEngine.interface';
import Interactor from './Interactor.interface';
import FileStorage from '../fileStorage/FileStorage.interface';

export default class OcrInteractor implements Interactor {
    constructor(
        private database: Database,
        private ocrEngine: OcrEngine,
        private fileStorage: FileStorage,
        private logger: Logger,
    ) {
        this.database = database;
        this.ocrEngine = ocrEngine;
        this.fileStorage = fileStorage;
        this.logger = logger;
    }

    public imageProcess = async (filename: string) => {
        this.logger.trace('imageProcess');

        const imageInfo = await this.fileStorage.getImageUrl(filename);
        const result = await this.ocrEngine.textDetection(imageInfo);
        await this.saveToDatabase(result);
        this.logger.trace('imageProcess %o', result.text);

        return result.text;
    };

    public saveToDatabase = async (result: { url: string; text: string }) => {
        this.logger.trace('saveToDatabase');
        await this.database.saveData(result);
    };

    public getResults = async () => {
        this.logger.trace('getResults');
        const results = await this.database.getData();
        this.logger.trace('getResults %o', results);
        return results;
    };
}
