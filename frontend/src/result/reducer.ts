import {IResultState} from './state'
import {IResultAction} from './actions'

const initialState: IResultState = {result: "Please upload an image with text.", route: "OCR", results: []}

export const resultReducer = (state: IResultState = initialState, action:IResultAction) => {
    switch(action.type){
        case 'GET_RESULT_SUCCESS':{
            return {
                ...state,
                result: action.result
            }
        }
        case 'PROCESSING':{
            return {
                ...state,
                result: "Image processing..."
            }
        }
        case 'GET_RESULT_FAIL':{
            return {
                ...state,
                result: action.error
            }
        }
        case "ROUTE_SELECTED" : {
            return {
                ...state,
                route: action.route
            }
        }

        case "GET_RESULTS_SUCCESS" : {
            return {
                ...state,
                results: action.results.slice()
            }
        }
        
        default:
            return state;
    }
}