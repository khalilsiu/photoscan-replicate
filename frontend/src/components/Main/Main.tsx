import React from 'react';
import styles from './Main.module.css';
import { useSelector, useDispatch } from 'react-redux';
import { IRootState } from '../../store';
import OCR from './OCR/OCR';
import { getResults } from '../../result/thunk';
import History from './History/History'

function Main() {
    const route = useSelector<IRootState, 'OCR' | 'history'>((state) => state.result.route);

    const dispatch = useDispatch()

    React.useEffect(()=> {
      dispatch(getResults())
    },[dispatch])

    const renderRoute = () => {
        if (route === 'OCR') return <OCR />;
        else return <History/>
    };

    return <div className={styles.main}>{renderRoute()}</div>;
}

export default Main;
